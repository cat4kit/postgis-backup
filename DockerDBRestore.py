import os
import subprocess

# Function to run shell commands
def run_command(command):
    print(f"Executing command: {command}")
    result = subprocess.run(command, shell=True, text=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if result.returncode != 0:
        print(f"Error executing command '{command}': {result.stderr}")
    else:
        print(result.stdout)
    return result
# To change the current branch to main
run_command('git checkout main')
run_command('git submodule update --init --recursive')
# Go to the directory where the docker-compose.yml file is located
os.chdir('..')


# docker-compose stop pgstacprod
# docker-compose --profile restore up -d
# docker-compose --profile restore down -v
# docker-compose up -d pgstacprod

# Creating backups
commands = [
    "docker-compose stop pgstacprod database",
    "docker-compose run --rm pgstacprod-restore",
    "docker-compose run --rm database-restore",
    "docker-compose up -d pgstacprod database",
]
for cmd in commands:
    run_command(cmd)
