import os
import subprocess

# Function to run shell commands
def run_command(command):
    print(f"Executing command: {command}")
    result = subprocess.run(command, shell=True, text=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if result.returncode != 0:
        print(f"Error executing command '{command}': {result.stderr}")
    else:
        print(result.stdout)
    return result
# To change the current branch to main
run_command('git checkout main')
# Go to the directory where the docker-compose.yml file is located
os.chdir('..')

# Creating backups
commands = [
    "docker-compose run --rm pgstacprod-backup",
    "docker-compose run --rm database-backup"
]
for cmd in commands:
    run_command(cmd)

# Change directory to the backup submodule
os.chdir('./backup')

# Run the git commands to add, commit, and push the backup to the submodule repository
# Git operations
git_commands = [
    "git add .",
    "git commit -m 'Updated backups'",
    "git push origin main"
]
for cmd in git_commands:
    run_command(cmd)

# Run the git commands to add, commit, and push the updated docker repository
#os.chdir('..')
# Git operations
#git_commands = [
#    "git add .",
#    "git commit -m 'Updated backups'",
#    "git push"
#]
#for cmd in git_commands:
#    run_command(cmd)
