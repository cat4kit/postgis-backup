# Makefile to configure backup and syncronization services
## Attention: Needs to be run via the root user

# Load .env file
ifneq (,$(wildcard ./.env))
    include .env
    export
endif

# Declare phony targets. 
.PHONY: config-crontab
# Target to configure the crontab for dbsyncpush
config-crontab:
	@echo "Configuring crontab for dbsyncpush"
	# Copy cron job file to the appropriate directory
	cp cron_dbsyncpush /etc/cron.d/cron_dbsyncpush
	# Set the correct file permissions
	chmod 0644 /etc/cron.d/cron_dbsyncpush
	# Install the new crontab file
	crontab /etc/cron.d/cron_dbsyncpush
	# Restart the cron service to apply changes
	service cron restart

#set the git configuration for username and password
.PHONY: git-config
git-config:
	@echo "Configuring git"
	git config --global user.username "$(GITLAB_USERNAME)"
	git config --global user.password "$(GITLAB_PASSWORD)"
